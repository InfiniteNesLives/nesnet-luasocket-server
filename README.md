# NESnet LuaSocket server

Some basic server tests using LuaSocket for NESnet project.
Starting from hacks of the provided example projects:  http://w3.impa.br/~diego/software/luasocket/home.html

What I did to get up and running on RPi: 
	Installed rasberian lite to SDcard using balenaEtcher
	booted Rpi with HDMI, keyboard, & ethernet
	set static IP using router settings 
	enabled ssh using sudo raspi-config (interfacing options)
	changed pi password
	created paul user and made sudo
	wget luasocket, but couldn't build, problem with depenency on lua.h
	couldn't easily figure out where the configuration setting was so tried luarocks
	sudo apt-get install luarocks
	That apparently installed luasocket as it now works:
	Lua 5.1.5  Copyright (C) 1994-2012 Lua.org, PUC-Rio
	> socket = require("socket")
	> print(socket._VERSION)
	LuaSocket 3.0-rc1

running examples may require sudo
