-----------------------------------------------------------------------------
-- UDP sample: echo protocol server
-- LuaSocket sample files
-- Author: Diego Nehab
-- RCS ID: $Id: echosrvr.lua,v 1.12 2005/11/22 08:33:29 diego Exp $
-----------------------------------------------------------------------------
local socket = require("socket")
--host = host or "127.0.0.1" --local machine
--host = host or "0.0.0.0" --any IP

port = port or 1234
if arg then
    host = arg[1] or host
    port = arg[2] or port
end
--print("Binding to host '" ..host.. "' and port " ..port.. "...")
udp = assert(socket.udp())

--assert(udp:setsockname(host, port)) --binds UDP object to local address

assert(udp:setsockname('*', port)) --If address is '*' the system binds to all local interfaces using the constant INADDR_ANY. If port is 0, the system chooses an ephemeral port.
assert(udp:settimeout(5))
ip, port = udp:getsockname()
assert(ip, port)
print("Waiting packets on " .. ip .. ":" .. port .. "...")

local timeout_cnt = 0
while 1 do
	dgram, ip, port = udp:receivefrom()
	if dgram then
		print("Echoing '" .. dgram .. "' to " .. ip .. ":" .. port)
		--udp:sendto(dgram, ip, port)
		udp:sendto("M   "..dgram, ip, port)
		if dgram == "exit\n" then
			return
		end
	else
		timeout_cnt = timeout_cnt+1
		if timeout_cnt > 20 then
			return
		end
    	end
        print(ip)
end
