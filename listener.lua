-----------------------------------------------------------------------------
-- TCP sample: Little program to dump lines received at a given port
-- LuaSocket sample files
-- Author: Diego Nehab
-- RCS ID: $Id: listener.lua,v 1.11 2005/01/02 22:44:00 diego Exp $
-----------------------------------------------------------------------------

local function hex(data)
	if data then
		--print("hex:", string.format("%X", data))
		return string.format("0x%X", data)
	end

	--[[
	if data then
	--	print("char:",data)
	--	data = tonumber(data)
	--	if data then
		        print("hex:", string.format("%X", data))
	--	end
	end
	--]]
end


local socket = require("socket")
host = host or "*"
port = port or 1234
if arg then
	host = arg[1] or host
	port = arg[2] or port
end
print("Binding to host '" ..host.. "' and port " ..port.. "...")
master = assert(socket.bind(host, port))
ipaddr, retport   = master:getsockname()
assert(ipaddr, retport)
print("Waiting connection from talker on " .. ipaddr .. ":" .. retport .. "...")
client = assert(master:accept())
print("Connected. Here is the stuff:")
msg, errmsg = client:receive()  --returns message, or nil & error

filename = "oam_dump.bin"

while not errmsg do

	print("len:", string.len(msg), "<"..msg..">")

	if (string.sub(msg,1,2) == "WP") then
		file = assert(io.open(filename, "wb"))
		msg, errmsg = client:receive(256)  --returns message, or nil & error
		file:write(msg)
		assert(file:close())
		return
	end

	if (string.sub(msg,1,2) == "RP") then
		print("1:", hex(string.byte(msg,1)))
		print("2:", hex(string.byte(msg,2)))
		print("3:", hex(string.byte(msg,3)))
		print("4:", hex(string.byte(msg,4)))
		--print("5:", hex(string.byte(msg,5)))
		--print("6:", hex(string.byte(msg,6)))

		--Request Page
		--local pagenum = string.byte(msg,4) + ((string.byte(msg,3))<<8) 
		local pagenum = string.byte(msg,3) + bit32.lshift(string.byte(msg,4),8) --little endian 
		if pagenum then
			print("NES requested page #", pagenum)
		else
			print("pagenum is nil")
		end
		return
	end

	msg, errmsg = client:receive()
end
print(errmsg)

